from math import sqrt

def f(n):
  """ Fibonacci, par la formule directe """
  phi = (1+sqrt(5)) /2
  phi2 = (1-sqrt(5)) /2
  return round(1/sqrt(5) * (phi**n-phi2**n))

